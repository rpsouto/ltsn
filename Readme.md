Development of Parallel version (OpenMO) of LTSn method for solving radiative transfer equation.

Requeriments:

- Intel MKL library (mandatory): free of charge from https://software.intel.com/en-us/performance-libraries

- Intel Paralell Studio (optional): https://software.intel.com/en-us/free-software



1) $ cp -r -p ./build.template/ build && cd ./build

2) source <parallel studio dir>/bin/psxevars.sh intel64

   OR
   
   source <intel mkl dir>/bin/mklvars.sh intel64
   
3) $ source ./compiler_intel.sh

   OR
   
   $ source ./compiler_gnu.sh
   
4) $ cmake ../

5) $ make

6) $ ./run-ltsn.sh 3 perfil11 80 50 173 128 1

