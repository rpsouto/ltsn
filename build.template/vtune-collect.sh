#!/bin/bash
# Usage: ./vtune-collect.sh <num mpi processes> <executable> <num openmp threads> <nElemComputed> <$NSIM>
#        ./vtune-collect.sh 3 perfil11 80 50 173 128 1

#source /opt/tools/intel/parallel_studio_xe_2018/bin/psxevars.sh intel64

DATE=$(date +%Y-%m-%d-%H%M%S)

executable=ltsn

wavelenght=${1}
profile=${2}
Nregions=${3}
Nmu=${4}
isotropyorder=${5}
lastiter=${6}
numthreads=${7}


namedir=perfanalysis/vtune/collect/${executable}/${profile}/OMP_${numthreads}/TIMESTART_$DATE
mkdir -p ${namedir}

export OMP_NUM_THREADS=${numthreads}

#amplxe: Error: Cannot start data collection because the scope of ptrace system call application is limited. To enable profiling, please set /proc/sys/kernel/yama/ptrace_scope to 0. See the Release Notes for instructions on enabling it permanently.

#Support for kernels 4.10 and newer is not available in Pin version of VTune Amplifier XE 2017 U3
#It will be included in future releases. You can try the env variable below to propagate the flag:
#AMPLXE_MORE_PIN_OPTIONS='-ifeellucky'
#export AMPLXE_MORE_PIN_OPTIONS='-ifeellucky'

amplxe-cl -collect hotspots -r ./${namedir} -- \
./${executable} ../data/clorofila/perfis/${profile}/parametros_hidro.dat ../data/clorofila/perfis/${profile}/clorofila_R${Nregions}.dat  ../data/clorofila/radiancias/${profile}/radiancias_R${Nregions}.dat ${Nregions} ${Nmu} ${wavelenght} ${isotropyorder} ${lastiter} \
2>&1 | tee output.log

mv output.log ./${namedir}/
cp ../data/clorofila/radiancias/${profile}/radiancias_R${Nregions}.dat ./${namedir}/
