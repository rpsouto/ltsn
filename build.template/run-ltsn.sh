# Usage:
# ./run-ltsn.sh <wavelength> <profile> <number of regions> <polar regions> <isotropy order> <number of azimuthal nodes iterations> <number of threads>
# ./run-ltsn.sh 3 perfil11 80 50 173 128 1

DATE=$(date +%Y-%m-%d-%H%M%S)

executable=ltsn

wavelenght=${1}
profile=${2}
Nregions=${3}
Nmu=${4}
isotropyorder=${5}
lastiter=${6}
numthreads=${7}

namedir=perfanalysis/${executable}/${profile}/OMP_${numthreads}/TIMESTART_$DATE
mkdir -p ${namedir}

export OMP_NUM_THREADS=${numthreads}

./${executable} \
	../data/clorofila/perfis/${profile}/parametros_hidro.dat \
	../data/clorofila/perfis/${profile}/clorofila_R${Nregions}.dat  \
	../data/clorofila/radiancias/${profile}/radiancias_R${Nregions}.dat \
	${Nregions} ${Nmu} ${wavelenght} ${isotropyorder} ${lastiter} 2>&1 | tee ltsn_output.txt

mv ltsn_output.txt ./${namedir}/
cp ../data/clorofila/radiancias/${profile}/radiancias_R${Nregions}.dat ./${namedir}/
