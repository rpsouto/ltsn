subroutine geraA(m,n,no,A,T,hl,omega,lv,pol,u,w)

implicit none

integer,intent(in) :: m
integer,intent(in) :: n
integer,intent(in) :: no
real(kind=8),dimension(no,no),intent(out) :: A
real(kind=8),dimension(lv+1),intent(in) :: hl
real(kind=8),intent(in) :: omega
integer,intent(in) :: lv
real(kind=8),dimension(lv+1,no+2),intent(in) ::pol
real(kind=8),dimension(no+1),intent(in) ::u
real(kind=8),dimension(no),intent(in) ::w
real(kind=8),dimension(no,no),intent(in) :: T
integer ::i,j

do j=1,no
   do i=1,no
      A(i,j)=(omega*w(j)*T(i,j)/2.0e+00)/u(i)
   enddo
   A(j,j)=(omega*w(j)*T(j,j)/2.0e+00-1.00e+00)/u(j)
enddo

end
