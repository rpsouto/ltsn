subroutine calc_intb(n,eigval,eigvec,x,L,mu,vet,b)

integer,intent(in)                           ::n

real(kind=8),intent(in),dimension(n)            ::eigval
real(kind=8),intent(in),dimension(n,n)            ::eigvec
real(kind=8),intent(in)                        ::x,mu
real(kind=8),intent(in)                        ::L
real(kind=8),intent(out),dimension(n)            ::vet
real(kind=8),dimension(n,n),intent(out)            ::b
real(kind=8)                              ::d,den

vet=0
d=dexp(-x/mu)
do i=1,n
   den=1.0d+00+mu*eigval(i)
   if(eigval(i).lt.0) then
      vet(i)=(dexp(eigval(i)*x)-d)/den
   else
      vet(i)=(dexp(eigval(i)*(x-L))-d*dexp(-L*eigval(i)))/den
   endif
enddo

do i=1,n
   do j=1,n
      b(i,j)=eigvec(i,j)*vet(j)
   enddo
enddo

end