program main

!Este programa resolve um problema anisotr�pico com fonte exponencial
! d/dx I(x) - A I(x) = F e^(-x/uo)
! A � gerado em Gera_A
! O vetor F � gerado em gera_Fonte (parte constante da fonte)


! Obs: Este programa est� adaptado para calcular as componentes de Fourier
! do problema sem simetria azimutal, assim quando m=0 o fluxo � dividido por 2
! No caso com simetria azimutal, fazer m=0, mas tirar esta divis�o.

implicit none

integer,parameter   :: ptos=10
integer,parameter   :: NM=11

double precision,allocatable :: hl(:)
double precision,allocatable :: cc0(:)
double precision,allocatable :: ccL(:)
double precision,allocatable :: u(:)
double precision,allocatable :: w(:)
double precision,allocatable :: z(:)
double precision,allocatable :: Chl(:)
double precision,allocatable :: saida_xpos(:)
double precision,allocatable :: saida_xneg(:)
double precision,allocatable  :: spr_valo_flu(:,:)

integer            :: iwl,lv,R,no,lastiter
double precision   :: wl(NM),a_w(NM),a_c(NM)

character(len=19)       :: anidir
character(len=7)        :: fnametemp
character(len=3)        :: iwlchar,rchar,nochar,lvchar,lastiterchar
character(len=200)      :: fileparam,filedata
character(len=200)      :: fileprofile,filewl

integer                     :: i,j,ptoss,ipath
double precision            :: pi,valor_tau
double precision            :: uo
double precision            :: phi

external solverLAPACK

anidir = '../dados/clorofila/'

 call getarg(1,fileparam) 
 call getarg(2,fileprofile) 
 call getarg(3,filedata) 
 call getarg(4,rchar)   
 call getarg(5,nochar)   
 call getarg(6,iwlchar)
 call getarg(7,lvchar)   
 call getarg(8,lastiterchar)

lvchar=trim(lvchar)
nochar=trim(nochar)
iwlchar=trim(iwlchar)
rchar=trim(rchar)
lastiterchar=trim(lastiterchar)
 
fnametemp = 'temp.dat'
write (fnametemp,*) lvchar
read (fnametemp,*) lv
write (fnametemp,*) lastiterchar
read (fnametemp,*) lastiter
write (fnametemp,*) rchar
read (fnametemp,*) R
write (fnametemp,*) nochar
read (fnametemp,*) no
write (fnametemp,*) iwlchar
read (fnametemp,*) iwl

print*, R, no, iwl, lv, lastiter

allocate(hl(lv+1))
allocate(cc0(no))
allocate(ccL(no))
allocate(u(no+1))
allocate(w(no))
allocate(z(R+1))
allocate(Chl(R+1))
allocate(saida_xpos(ptos))
allocate(saida_xneg(ptos))
allocate(spr_valo_flu(2*ptos,R+1))


filewl = '../dados/clorofila/eq327_m11.dat'
fileparam = TRIM(fileparam)
fileprofile = TRIM(fileprofile)
filedata = TRIM(filedata)


OPEN (21, FILE = fileparam)
READ (21, *) j
READ (21, *) cc0(1), ccL(1), valor_tau
READ (21, *) uo
READ (21, *) ptoss

READ (21, *) (saida_xpos(i), i = 1, ptos)
DO i = 1, ptos
   saida_xneg(i) = - saida_xpos(ptos+1-i)
ENDDO

OPEN (21, FILE = fileprofile)
DO i = 1, max(R+1,2)
   READ (21,*) z(i),Chl(i)
   WRITE(*,"(2E13.5)") z(i),Chl(i)
ENDDO
CLOSE(21)
         
OPEN (21, FILE = filewl)
DO i = 1, NM
   READ (21, *) wl(i),a_w(i),a_c(i)
ENDDO
CLOSE(21)

ipath=1
pi=3.141593d+00   
if (j==1) then
   phi=0.0d+00
elseif(j==2) then
   phi=pi/2.0d+00
else
   phi=pi
endif
!uo=0.5d+00                        ! expoente da fonte e^(-x/uo)
!open (unit=20, file='saida_hidro')         ! saida da respota sai.dat
!write(20,*) 'N=',no,'Grau de anisotropia, L =',lv+1
!write(20,*) 'm=',m
!Alocando Matrizes e Vetores 
!Definindo as CContorno nos vetores de ordem N/2 cc0 e ccL

 cc0 = 0.0d+00
 ccL = 0.0d+00


if(lv==0) then
   open (unit=8,file=anidir//'legendre/hl_0.dat',status='old')
elseif(lv==8) then
   open (unit=8,file=anidir//'legendre/hl_8.dat',status='old')      !arquivo dos Bl da anisotropia, B0=1
elseif(lv==82) then
   open (unit=8,file=anidir//'legendre/hl_82.dat',status='old')
elseif(lv==173) then
   open (unit=8,file=anidir//'legendre/hl_173.dat',status='old')
elseif(lv==299) then
   open (unit=8,file=anidir//'legendre/hl_299.dat',status='old')
endif
do i=1,lv+1
   read(8,*) hl(i)
enddo
close(8)

call galeg(no,-1.d+00,1.d+00,u,w)            ! calcula pesos e raizes da quadratura
u(no+1)=uo

call solverLAPACK(R,no,lv,NM,iwl,phi,hl,cc0,ccL,uo,u,w,z,Chl,wl,a_w,a_c,lastiter,ptos,saida_xneg,saida_xpos,spr_valo_flu)

OPEN(22,FILE=filedata,STATUS="UNKNOWN", &
               ACCESS="SEQUENTIAL",ACTION="WRITE")
DO i = 1, ptos
   WRITE (22, "(90E12.3)") (spr_valo_flu(i,j), j=1,R+1)
ENDDO
DO i = ptos+1,2*ptos
   WRITE (22, "(90E12.3)") (spr_valo_flu(i,j), j=1,R+1)
ENDDO
CLOSE(22)

deallocate(hl)
deallocate(cc0)
deallocate(ccL)
deallocate(u)
deallocate(w)
deallocate(z)
deallocate(Chl)
deallocate(saida_xpos)
deallocate(saida_xneg)
deallocate(spr_valo_flu)

end program main




