subroutine matrizT(m,no,T,hl,lv,pol,u,w)

implicit none

integer,intent(in) :: m
integer,intent(in) :: no
integer,intent(in) :: lv
real(kind=8),dimension(lv+1),intent(in) :: hl
real(kind=8),dimension(lv+1,no+2),intent(in) ::pol
real(kind=8),dimension(no+1),intent(in) ::u
real(kind=8),dimension(no),intent(in) ::w
real(kind=8),dimension(no,no),intent(inout) :: T
integer :: i,j,l,n 

n=lv-m+1

do j=1,no
   do i=1,no
      T(i,j) = 0.0d+00
      do l=1,n
         T(i,j) = T(i,j) + hl(l+m)*pol(l+m,i)*pol(l+m,j)
      enddo
   enddo
enddo

end

