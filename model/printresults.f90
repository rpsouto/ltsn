SUBROUTINE printresults

USE generalparams
USE iovars

implicit none

integer i,j

!spr_valo_flu(11:20,1)=0.0d+00
!spr_valo_flu(1:10,R+1)=0.0d+00

OPEN(22,FILE=filedata,STATUS="UNKNOWN", &
               ACCESS="SEQUENTIAL",ACTION="WRITE")
DO i = 1, ptos
   WRITE (22, "(90D12.3)") (spr_valo_flu(i,j), j=1,R+1)
ENDDO
DO i = ptos+1,2*ptos
   WRITE (22, "(90D12.3)") (spr_valo_flu(i,j), j=1,R+1)
ENDDO

CLOSE(22)

deallocate(hl)
deallocate(cc0)
deallocate(ccL)
deallocate(u)
deallocate(w)
deallocate(z)
deallocate(Chl)
deallocate(saida_xpos)
deallocate(saida_xneg)
deallocate(spr_valo_flu)


END SUBROUTINE printresults