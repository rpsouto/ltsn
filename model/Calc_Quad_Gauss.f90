!     SUBROUTINE GALEG - CALCULA OS PESOS E RA�ZES GAUSS-LEGENDRE

subroutine galeg(n,xe,xd,x,w)

implicit none

integer                              ::n
integer                              ::i
integer                              ::j
integer                              ::m

double precision                     ::a
double precision                     ::eps
double precision                     ::p1
double precision                     ::p2
double precision                     ::p3
double precision                     ::pi
double precision                     ::pp
double precision,dimension(n+1)            ::x
double precision                     ::xd
double precision                     ::xe
double precision                     ::xl
double precision                     ::xm
double precision,dimension(n)            ::w
double precision                     ::z
double precision                     ::z1

EPS=3.0d-16
Pi=3.141592653589793238d+00
xl=(xd-xe)/2.0d+00
xm=(xd+xe)/2.0d+00
m=(n+1)/2
do i=1,m
   z=cos(PI*(i-0.25d+00)/(n+0.25d+00))
      200   p1=1.0d+00
      p2=0.0d+00
      do j=1,n
         p3=p2
         p2=p1
         p1=((2.0*j-1.0d+00)*z*p2-(j-1.0d+00)*p3)/j
      enddo                                
      pp=n*(z*p1-p2)/(z*z-1.0d+00)
      z1=z
      z=z1-p1/pp
      a=dabs(z-z1)
   if(a.gt.eps) go to 200
   x(i)=xm+xl*z
   x(n+1-i)=xm-xl*z
   w(i)=2.0d+00*xl/((1.0d+00-z*z)*pp*pp)
   w(n+1-i)=w(i)
enddo
end