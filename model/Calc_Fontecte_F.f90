subroutine gera_Fonte(m,no,hl,omega,lv,pol,u,fonte)
!calcula a parte constante da fonte, i� calcula o vetor Fonte de 
!Fonte*exp(-1/uo *x)

implicit none

integer                              ::no
integer                              ::lv

double precision,dimension(no)            ::Fonte

double precision                     ::cte2
double precision                     ::cte1
double precision,dimension(lv+1)         ::hl
double precision                     ::omega
double precision,dimension(lv+1,no+2)      ::pol
double precision                     ::pp
double precision                     ::soma
double precision,dimension(no+1)         ::u


integer                              ::i,l,m

if(m==0) then
   cte2=omega/4.0d+00
   !cte2=omega/2.0d+00
else
   cte2=omega/2.0d+00
endif
do i=1,no
   cte1=1/u(i)
   soma=0
   do l=m+1,lv+1
      pp=pol(l,i)*pol(l,no+1)
      soma=soma+hl(l)*pp
   enddo
   fonte(i)=cte2*cte1*soma
enddo
!do i=1,no+1
!   write(80,*) i,fonte(i)
!enddo

end