subroutine fluxo_angular(n,v,b,fonte,fluxo)

integer,intent(in)                           ::n

real(kind=8),intent(in),dimension(n)            ::v
real(kind=8),intent(in),dimension(n)            ::fonte
real(kind=8),intent(in),dimension(n,n)            ::b
real(kind=8),dimension(n)                     ::aux1
real(kind=8),intent(out),dimension(n)            ::fluxo

aux1(1:n)=Matmul(b(1:n,1:n),v(1:n))

fluxo=aux1+fonte

end