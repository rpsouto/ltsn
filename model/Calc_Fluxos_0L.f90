subroutine Fluxo_0L(n,CI,CC0,CCL,F0,FL)

integer,intent(in)                           ::n

real(kind=8),dimension(n),intent(in)            ::CI
real(kind=8),intent(in),dimension(n)            ::CC0
real(kind=8),intent(in),dimension(n)            ::CCL
real(kind=8),intent(out),dimension(n)            ::F0
real(kind=8),intent(out),dimension(n)            ::FL

do i=1,n/2
   F0(i)=CC0(i)
   F0(i+n/2)=CI(i+n/2)
   FL(i)=CI(i)
   FL(i+n/2)=CCL(i)
enddo

end