!     *********************************************************************
!     *        SUBROUTINE PNASS - C�LCULO DE P SUB M+K SUP M ( MU(J))     *
!     *                  k=1,2,...,8 , j=1,...,no+2                    *
!     *Esta subrotina calcula o valos do polin�mios associados de Legendre*
!     *com m dado, a nota��o usada �:  P_m+k^m(mu_j)=pol(m+1+k,j)         *
!     *********************************************************************
subroutine pnass(m,u,lv,no,pol)

implicit none

integer,intent(in)                           ::no,m,lv
integer                                      ::i,jj,k,mm

double precision                             ::aux1,aux2,aux3,aux4
double precision,dimension(no+1),intent(in)  ::u
double precision                             :: p1,p2
double precision,dimension(lv+1,no+2),intent(out)   ::pol
double precision                             ::prodi,prodp
double precision                             ::ii,ip
!     
!c�lculo de P sub m sup m (u(jj))
!
if(lv.eq.0) then
      pol=1.0d+00
else
   mm=m+1
   do jj=1,no+2
      prodp=1.0d+00
      prodi=1.0d+00
      !write(*,*) '1 - m=',m
      p1 = 1.0d+00
      do i=2,2*m+2,2
          !write(*,*) 'i, prodp, prodi=',i, prodp, prodi
         ip=dfloat(i)
         ii=dfloat(i-1)
         p1 = p1*dsqrt(ii)/dsqrt(ip)
         !prodp=prodp*dsqrt(ip)
         !prodi=prodi*dsqrt(ii)
      enddo
      !write(*,*) '2 - m=',m
      !p1=dsqrt(ip)*prodi/(dsqrt(ii)*prodp)
      p1=dsqrt(ip)*p1/dsqrt(ii)
      p2=(dsqrt(1.0d+00-u(jj)*u(jj)))**m
      pol(mm,jj)=p1*p2
      !c�lculo dos P sub m+k sup m (u(j))
      pol(mm+1,jj)=dsqrt(2.0d+00*dfloat(m)+1.0d+00)*u(jj)*pol(mm,jj)
      !do k=1,lv-1 !invasao de area!!!
      do k=1,lv-1-mm
         aux1=(2*dfloat(m+k)+1)*u(jj)*pol(mm+k,jj)
         aux2=dsqrt(dfloat((2*m+k)*k))*pol(mm+k-1,jj)
         aux3=dfloat((2*m+k+1)*(k+1))
         aux4=dsqrt(aux3)
         pol(mm+k+1,jj)=(aux1-aux2)/aux4
      enddo
   enddo
endif
return
end
