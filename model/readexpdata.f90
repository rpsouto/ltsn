SUBROUTINE readexpdata

USE generalparams
USE iovars

anidir = '../data/clorofila/'

 call getarg(1,fileparam) 
 call getarg(2,fileprofile) 
 call getarg(3,filedata) 
 call getarg(4,rchar)   
 call getarg(5,nochar)   
 call getarg(6,iwlchar)
 call getarg(7,lvchar)   
 call getarg(8,lastiterchar)

lvchar=trim(lvchar)
nochar=trim(nochar)
iwlchar=trim(iwlchar)
rchar=trim(rchar)
lastiterchar=trim(lastiterchar)
 
fnametemp = 'temp.dat'
write (fnametemp,*) lvchar
read (fnametemp,*) lv
write (fnametemp,*) lastiterchar
read (fnametemp,*) lastiter
write (fnametemp,*) rchar
read (fnametemp,*) R
write (fnametemp,*) nochar
read (fnametemp,*) no
write (fnametemp,*) iwlchar
read (fnametemp,*) iwl

allocate(hl(lv+1))
allocate(cc0(no))
allocate(ccL(no))
allocate(u(no+1))
allocate(w(no))
allocate(z(R+1))
allocate(Chl(R+1))
allocate(saida_xpos(ptos))
allocate(saida_xneg(ptos))
allocate(spr_valo_flu(2*ptos,R+1))


filewl = '../data/clorofila/eq327_m11.dat'
fileparam = TRIM(fileparam)
fileprofile = TRIM(fileprofile)
filedata = TRIM(filedata)


OPEN (21, FILE = fileparam)
READ (21, *) j
READ (21, *) cc0(1), ccL(1), valor_tau
READ (21, *) uo
READ (21, *) ptoss

READ (21, *) (saida_xpos(i), i = 1, ptos)
DO i = 1, ptos
   saida_xneg(i) = - saida_xpos(ptos+1-i)
ENDDO

OPEN (21, FILE = fileprofile)
DO i = 1, max(R+1,2)
   READ (21,*) z(i),Chl(i)
   !WRITE(*,"(2E13.5)") z(i),Chl(i)
ENDDO
CLOSE(21)
         
OPEN (21, FILE = filewl)
DO i = 1, NM
   READ (21, *) wl(i),a_w(i),a_c(i)
ENDDO
CLOSE(21)

ipath=1
pi=3.141593d+00   
if (j==1) then
   phi=0.0d+00
elseif(j==2) then
   phi=pi/2.0d+00
else
   phi=pi
endif
!uo=0.5d+00                        ! expoente da fonte e^(-x/uo)
!open (unit=20, file='saida_hidro')         ! saida da respota sai.dat
!write(20,*) 'N=',no,'Grau de anisotropia, L =',lv+1
!write(20,*) 'm=',m
!Alocando Matrizes e Vetores 
!Definindo as CContorno nos vetores de ordem N/2 cc0 e ccL

 cc0 = 0.0d+00
 ccL = 0.0d+00


if(lv==0) then
   open (unit=8,file=anidir//'legendre/hl_0.dat',status='old')
elseif(lv==8) then
   open (unit=8,file=anidir//'legendre/hl_8.dat',status='old')      !arquivo dos Bl da anisotropia, B0=1
elseif(lv==82) then
   open (unit=8,file=anidir//'legendre/hl_82.dat',status='old')
elseif(lv==173) then
   open (unit=8,file=anidir//'legendre/hl_173.dat',status='old')
elseif(lv==299) then
   open (unit=8,file=anidir//'legendre/hl_299.dat',status='old')
endif
do i=1,lv+1
   read(8,*) hl(i)
enddo
close(8)

call galeg(no,-1.d+00,1.d+00,u,w)            ! calcula pesos e raizes da quadratura
u(no+1)=uo

END SUBROUTINE readexpdata

