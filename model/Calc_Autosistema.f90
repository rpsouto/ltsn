subroutine calc_dados(n,mat_a,eigval,eigvec,inv_eigvec)
!subroutine spr_calc_eig(mat_a,eigvec,eigval)

!use msimsl
implicit none

integer,intent(in)                  ::n
real(kind=8),intent(in),dimension(n,n)   :: mat_a
real(kind=8),intent(out),dimension(n,n)   :: eigvec
real(kind=8),intent(out),dimension(n)   :: eigval
real(kind=8),intent(out),dimension(n,n)   ::inv_eigvec


!------------------------------------------------
! Declaracao dos parametros da DGEEV
! Na ordem em que aparecem na chamada da subrotina
!------------------------------------------------


character(len=1)         :: jobvl='V'
character(len=1)         :: jobvr='V'

integer               :: lda

real(kind=8),dimension(n):: wr
real(kind=8),dimension(n):: wi
real(kind=8),dimension(n,n):: vl
real(kind=8),dimension(n,n):: aux

integer               :: ldvl

real(kind=8),dimension(n,n):: vr

integer               :: ldvr

real(kind=8),dimension(:),allocatable   :: work

integer               :: lwork
integer               :: info

integer      :: dummy

lda=n
ldvr=n
ldvl=n
lwork=34*n

allocate(work(lwork))
aux=mat_a
call dgeev(jobvl,jobvr,n,aux,lda,wr,wi,vl,ldvl,vr,ldvr,work,lwork,info)
!eigval=wr
!eigvec=vr

eigval=wr
eigvec=vr
inv_eigvec=vr

call inv_autovet(n,inv_eigvec)

deallocate(work)
end subroutine calc_dados
