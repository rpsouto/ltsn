!Este programa resolve um problema anisotrópico com fonte exponencial
! d/dx I(x) - A I(x) = F e^(-x/uo)
! A é gerado em Gera_A
! O vetor F é gerado em gera_Fonte (parte constante da fonte)


! Obs: Este programa está adaptado para calcular as componentes de Fourier
! do problema sem simetria azimutal, assim quando m=0 o fluxo é dividido por 2
! No caso com simetria azimutal, fazer m=0, mas tirar esta divisão.
           
subroutine solverSparse(R,no,lv,NM,iwl,phi,hl,cc0,ccL,uo,u,w,z,Chl,wl,a_w,a_c,lastiter,ptos,saida_xneg,saida_xpos,spr_valo_flu)

implicit none

integer, intent(in) :: R
integer, intent(in) :: no
integer, intent(in) :: lv
integer, intent(in) :: NM
integer, intent(in) :: iwl
double precision, intent(in) :: phi
double precision, intent(in) :: hl(lv+1)
double precision, intent(in) :: cc0(no)
double precision, intent(in) :: ccL(no)
double precision, intent(in) :: uo
double precision, intent(in) :: u(no+1)
double precision, intent(in) :: w(no)
double precision, intent(in) :: z(R+1)
double precision, intent(in) :: Chl(R+1)
double precision, intent(in)  :: wl(NM),a_w(NM),a_c(NM)
integer, intent(in) :: lastiter
integer, intent(in) :: ptos
double precision :: saida_xpos(ptos)
double precision :: saida_xneg(ptos)
double precision, intent(inout) :: spr_valo_flu(2*ptos,R+1)


integer :: N, nnz, offset, id_offset, omp_get_thread_num,id_rowptr

double precision,allocatable :: values(:)
!integer,allocatable :: rowind(:)
integer,allocatable :: colind(:)
integer,allocatable :: rowptr(:)
double precision,allocatable :: T(:,:)
double precision,allocatable :: b0(:,:)
double precision,allocatable :: bL(:,:)
double precision,allocatable :: Big_b0(:,:,:)
double precision,allocatable :: Big_bL(:,:,:)
double precision,allocatable :: L(:)
double precision,allocatable :: LL(:)
double precision,allocatable :: omega(:)
double precision,allocatable :: pol(:,:)
double precision,allocatable :: VetorSist(:)
double precision,allocatable :: VetorSist_Aux(:)
double precision,allocatable :: A(:,:)
double precision,allocatable :: eigval(:)
double precision,allocatable :: eigvec(:,:)
double precision,allocatable :: inv_eigvec(:,:)
double precision,allocatable :: Hh0(:)
double precision,allocatable :: HhL(:)
double precision,allocatable :: Big_Hh0(:,:)
double precision,allocatable :: Big_HhL(:,:)
double precision,allocatable :: fonte(:)
double precision,allocatable :: Fluxo_em_0(:)
double precision,allocatable :: soma0(:,:)
double precision,allocatable :: Fluxo_em_L(:)
double precision,allocatable :: somaL(:,:)
double precision,allocatable :: ap(:)
double precision,allocatable :: bp(:)
double precision,allocatable :: cp(:)
double precision,allocatable :: abcissa_pos(:)
double precision,allocatable :: abcissa_neg(:)
double precision,allocatable :: ordenada_pos(:)
double precision,allocatable :: ordenada_neg(:)
double precision,allocatable :: saida_ypos(:)
double precision,allocatable :: saida_yneg(:)


double precision   :: part1,part2,cte
integer                     :: i,m,j,ii,jj,ij,ji
integer                     :: mm,ipath
double precision            :: pi,valor_tau

integer                     :: k,kk,pos,ptoss,div!,ptos


!Definicao de parametros do solver do sistema

! allocate ( values((2*R-1)*no*no) )
! !allocate ( rowind((2*R-1)*no*no) )
! allocate ( colind((2*R-1)*no*no) )
! allocate ( rowptr(R*no+1) )
! allocate ( T(no,no) )
! allocate ( pol(lv+1,no+2) )
! allocate ( VetorSist(R*no) )
! allocate ( VetorSist_Aux(no) )
! allocate ( A(no,no) )
! allocate ( eigval(no) )
! allocate ( eigvec(no,no) )
! allocate ( inv_eigvec(no,no) )
! allocate ( Hh0(no) )
! allocate ( HhL(no) )
! allocate ( b0(no,no) )
! allocate ( bL(no,no) )
! allocate ( fonte(no) )
! allocate ( Fluxo_em_0(no) )
! allocate ( Fluxo_em_L(no) )
allocate ( soma0(R,no) )
allocate ( somaL(R,no) )
allocate ( ap(R) )
allocate ( bp(R) )
allocate ( cp(R) )
allocate ( abcissa_pos(no/2) )
allocate ( abcissa_neg(no/2) )
allocate ( ordenada_pos(no/2) )
allocate ( ordenada_neg(no/2) )
allocate ( saida_ypos(ptos) )
allocate ( saida_yneg(ptos) )

allocate ( L(R) )
allocate ( LL(R+1) )
allocate ( omega(R) )


N = R*no
nnz = (2*R-1)*no*no

!u(no+2)=0.0
kk=iwl
 part2 = 1 + 0.2*dexp(-0.014*(wl(kk) - 440.0))
 cte = (550.0/wl(kk)) * 0.30
 DO i = 1, R
    part1 = a_w(kk) + 0.06*a_c(kk)*(Chl(i)**0.65)
    ap(i) = part1*part2
    bp(i) = cte*(Chl(i)**0.62)
    cp(i) = ap(i) + bp(i)
    omega(i) = bp(i) / cp(i)
    L(i) = cp(i)*(z(i+1)-z(i))
 ENDDO

 LL(1)=0.0d+00
 IF (R .GT. 1) THEN
    do i=2,R+1
       LL(i)=LL(i-1)+L(i-1)
    enddo
 ELSE
    LL(2) = L(1)
 ENDIF


print*, 'Montando o Sistema...'

! This force computation for each azimuthal node is performed in parallel 
!$OMP PARALLEL                    &
!$OMP DEFAULT(none)               &    
!$OMP PRIVATE(T,b0,bL,pol,VetorSist_Aux,A,offset,id_offset,id_rowptr,values,rowptr,colind,VetorSist) &
!$OMP PRIVATE(eigval,eigvec,inv_eigvec,Hh0,HhL,fonte,Fluxo_em_0,Fluxo_em_L,m,i,j,ii,jj,ij,ji)  &
!$OMP PRIVATE(Big_b0,Big_bL,Big_Hh0,Big_HhL)  &
!$OMP SHARED(lastiter,phi,no,R,lv,u,uo,w,L,LL,omega,hl,cc0,ccL,N,nnz) &
!$OMP REDUCTION(+ : soma0, somaL)

!Initialize private arrays
allocate ( values((2*R-1)*no*no) )
allocate ( colind((2*R-1)*no*no) )
allocate ( rowptr(R*no+1) )
allocate ( T(no,no) )
allocate ( pol(lv+1,no+2) )
allocate ( VetorSist(R*no) )
allocate ( VetorSist_Aux(no) )
allocate ( A(no,no) )
allocate ( eigval(no) )
allocate ( eigvec(no,no) )
allocate ( inv_eigvec(no,no) )
allocate ( Hh0(no) )
allocate ( HhL(no) )
allocate ( Big_Hh0(R,no) )
allocate ( Big_HhL(R,no) )
allocate ( b0(no,no) )
allocate ( bL(no,no) )
allocate ( Big_b0(R,no,no) )
allocate ( Big_bL(R,no,no) )
allocate ( fonte(no) )
allocate ( Fluxo_em_0(no) )
allocate ( Fluxo_em_L(no) )

T=0d+00
b0=0d+00
bL=0d+00
pol=0d+00
VetorSist_Aux=0d+00
A=0d+00
VetorSist=0.0d+00
values=0.0d+00
 colind=0
rowptr=0


!Initialize reduction arrays
soma0=0d+00
somaL=0d+00

!$OMP DO                    
DO m=0,lastiter-1

   print*, 'm, tid: ',m, omp_get_thread_num()

   call pnass(m,u,lv,no,pol)                  ! calcula os polinomios associados de Legendre
   call matrizT(m,no,T,hl,lv,pol,u,w)

   call geraA(m,n,no,A,T,hl,omega(1),lv,pol,u,w)      ! gera a matriz A de sI-A
   call gera_Fonte(m,no,hl,omega(1),lv,pol,u,fonte)   ! Adaptar a cada caso, gera o vetor cte da fonte
   call calc_dados(no,A,eigval,eigvec,inv_eigvec)   ! Autovalores, Autovetores, Inversa dos Autovetores
   call calc_b(no,eigval,eigvec,0.0d+00,L(1),b0)      ! B(0)=X Exp[x* lambda]
   call convolucao(no,eigval,eigvec,inv_eigvec,fonte,uo,0.0d+00,L(1),Hh0)
   
   offset=0
   id_offset=0
 
   id_rowptr=0
   do i=1, no/2
      id_rowptr = id_rowptr + 1
      rowptr(id_rowptr) = (i-1)*no + 1 
      do j=1, no
         values((i-1)*no + j ) = b0(i,j)
         !rowind((i-1)*no + j ) = i
         colind((i-1)*no + j ) = j
      enddo
   enddo
   do i=1, no/2
      VetorSist(i)=cc0(i)-Hh0(i)
   enddo
   do i=1,no
      do j=1,no
        Big_b0(1,i,j)=b0(i,j)
      enddo
   enddo
   do i=1,no
      Big_Hh0(1,i)=Hh0(i)
   enddo

   do jj=2,R
      call calc_b(no,eigval,eigvec,L(jj-1),L(jj-1),bL)            ! BL(L) e BNeg(L)
      call convolucao(no,eigval,eigvec,inv_eigvec,fonte,uo,LL(jj),LL(jj),HhL)
      do ij=1,no
         id_rowptr = id_rowptr + 1
         rowptr(id_rowptr) = no*no/2 + (jj-2)*(no)*(2*no) + (ij-1)*(2*no) + 1 
         do ji=1,no
            values(no*no/2 + (jj-2)*(no)*(2*no) + (ij-1)*(2*no) + ji ) = bL(ij,ji)
            !rowind(no*no/2 + (jj-2)*(no)*(2*no) + (ij-1)*(2*no) + ji ) = no/2+(jj-2)*no+ij
            colind(no*no/2 + (jj-2)*(no)*(2*no) + (ij-1)*(2*no) + ji ) = (jj-2)*no+ji
         enddo
      enddo
      call geraA(m,n,no,A,T,hl,omega(jj),lv,pol,u,w)     ! gera a matriz A de sI-A
      call gera_Fonte(m,no,hl,omega(jj),lv,pol,u,fonte)   ! Adaptar a cada caso, gera o vetor cte da fonte
      call calc_dados(no,A,eigval,eigvec,inv_eigvec)   ! Autovalores, Autovetores, Inversa dos Autovetores
      call calc_b(no,eigval,eigvec,0.0d+00,L(jj),b0)      ! B(0)=X Exp[x* lambda]
      call convolucao(no,eigval,eigvec,inv_eigvec,fonte,uo,LL(jj),LL(jj+1),Hh0)
      do ij=1,no
         do ji=1,no
            values(no*no/2 + (jj-2)*(no)*(2*no) + (ij-1)*(2*no) + ji + no ) = -b0(ij,ji)
            !rowind(no*no/2 + (jj-2)*(no)*(2*no) + (ij-1)*(2*no) + ji + no ) = no/2+(jj-2)*no+ij
            colind(no*no/2 + (jj-2)*(no)*(2*no) + (ij-1)*(2*no) + ji + no ) = (jj-1)*no+ji            
         enddo
      enddo
      do ii=1,no
         VetorSist(no/2+(jj-2)*no+ii)=Hh0(ii)-HhL(ii)
      enddo
      do i=1,no
         do j=1,no
            Big_b0(jj,i,j)=b0(i,j)
         enddo
      enddo
      do i=1,no
         big_Hh0(jj,i)=Hh0(i)
      enddo
   enddo
   
   call calc_b(no,eigval,eigvec,L(R),L(R),bL)            ! BL(L) e BNeg(L)
   call convolucao(no,eigval,eigvec,inv_eigvec,fonte,uo,LL(R+1),LL(R+1),HhL)
   do ij=1,no/2
      id_rowptr = id_rowptr + 1
      rowptr(id_rowptr) = no*no/2 + (R-1)*(no)*(2*no) + (ij-1)*no + 1 
      do ji=1,no
         values(no*no/2 + (R-1)*(no)*(2*no) + (ij-1)*no + ji ) = bL(ij+no/2,ji)
         !rowind(no*no/2 + (R-1)*(no)*(2*no) + (ij-1)*no + ji ) = R*no-no/2+ij
         colind(no*no/2 + (R-1)*(no)*(2*no) + (ij-1)*no + ji ) = R*no-no+ji
      enddo
   enddo
   do ii=1,no/2
      VetorSist(R*no-no/2+ii)=ccL(ii+no/2)-HhL(ii+no/2)
   enddo 
   do i=1,no
      do j=1,no
         Big_bL(R,i,j)=bL(i,j)
      enddo
   enddo
   do i=1,no
      Big_HhL(R,i)=HhL(i)
   enddo
 
   rowptr(N+1)=nnz+1 

   !print*, 'Resolve o sistema esparso com PARDISO'
   CALL pardiso_unsym(N, nnz, rowptr, colind, values, VetorSist)

   do ij=1,no
      do ji=1,no
        b0(ij,ji)=Big_b0(1,ij,ji)
     enddo
     Hh0(ij)=Big_Hh0(1,ij)
   enddo
   
   VetorSist_Aux = VetorSist(1:no)
   call Fluxo_angular(no,VetorSist_Aux,b0,Hh0,Fluxo_em_0)
   do ii=1,no
      fluxo_em_0(ii)=fluxo_em_0(ii)*dcos(m*phi)
   enddo
   soma0(1,:)=soma0(1,:) + fluxo_em_0

   !do i=2,R
   do i=1,R-1
      do ij=1,no
         do ji=1,no
            bL(ij,ji)=Big_bL(i,ij,ji)
            b0(ij,ji)=Big_b0(i+1,ij,ji)
         enddo
         HhL(ij)=Big_HhL(i,ij)
         Hh0(ij)=Big_Hh0(i+1,ij)
      enddo
      
      VetorSist_Aux=VetorSist((i-1)*no+1:i*no)
      call Fluxo_angular(no,VetorSist_Aux,bL,HhL,Fluxo_em_L)

      VetorSist_Aux=VetorSist(i*no+1:(i+1)*no)
      call Fluxo_angular(no,VetorSist_Aux,b0,Hh0,Fluxo_em_0)
      
      do ii=1,no
         fluxo_em_L(ii)=fluxo_em_L(ii)*dcos(m*phi)
         fluxo_em_0(ii)=fluxo_em_0(ii)*dcos(m*phi)
      enddo
      somaL(i,:) = somaL(i,:) + fluxo_em_L
      soma0(i+1,:) = soma0(i+1,:) + fluxo_em_0
   enddo

   do ij=1,no
      do ji=1,no
         bL(ij,ji)=Big_bL(R,ij,ji)
      enddo
      HhL(ij)=Big_HhL(R,ij)
   enddo

   fluxo_em_L=0
   VetorSist_Aux=VetorSist(R*no-no+1:R*no)
   call Fluxo_angular(no,VetorSist_Aux,bL,HhL,Fluxo_em_L)
   !fluxo_em_L(no/2+1:no) = Matmul(bL(no/2+1:no,:),Sai_Gauss_Aux) + HhL(no/2+1:no)
   !fluxo_em_L(1:no/2) = Matmul(bL(1:no/2,:),Sai_Gauss_Aux) + HhL(1:no/2)
   do ii=1,no
      fluxo_em_L(ii)=fluxo_em_L(ii)*dcos(m*phi)
   enddo
   somaL(R,:) = somaL(R,:) + fluxo_em_L
    !write(*,*) 'm, regiao, soma : ',m ,R, sum(fluxo_em_L(no/2+1:no))

enddo ! Fim do laco azimutal
!$OMP END DO

deallocate ( values )
deallocate ( colind )
deallocate ( rowptr )
deallocate ( T )
deallocate ( pol )
deallocate ( VetorSist )
deallocate ( VetorSist_Aux )
deallocate ( A )
deallocate ( eigval )
deallocate ( eigvec )
deallocate ( inv_eigvec )
deallocate ( Hh0 )
deallocate ( HhL )
deallocate ( Big_Hh0 )
deallocate ( Big_HhL )
deallocate ( b0 )
deallocate ( bL )
deallocate ( Big_b0 )
deallocate ( Big_bL )
deallocate ( fonte )
deallocate ( Fluxo_em_0 )
deallocate ( Fluxo_em_L )

!$OMP END PARALLEL

!----------------------------------------------------------------
! Aqui eu chamo os mus continuos, via interpolacao 433
! Detalhe importante eh que eu tenho que dividir tudo em
! arrays auxiliares para mandar para dentro da rotina. Na
! volta eu junto tudo de novo
!----------------------------------------------------------------

div=no/2

do j=1,div
   abcissa_pos(j)=u(div-j+1)
   abcissa_neg(j)=u(no-j+1)
end do
do k=1,R
   do j=1,div
      pos=div-j+1
      ordenada_pos(j)=soma0(k,pos)
      ordenada_neg(j)=soma0(k,no-j+1)
   end do
   call intrpl(2,div,abcissa_pos,ordenada_pos,ptos,saida_xpos,saida_ypos)
   call intrpl(2,div,abcissa_neg,ordenada_neg,ptos,saida_xneg,saida_yneg)
   do j=1,ptos
      spr_valo_flu(j,k)=saida_yneg(j)
      spr_valo_flu(j+ptos,k)=saida_ypos(j)
   end do
end do
do j=1,div
   pos=div-j+1
   ordenada_pos(j)=somaL(R,pos)
   ordenada_neg(j)=somaL(R,no-j+1)
end do
call intrpl(2,div,abcissa_pos,ordenada_pos,ptos,saida_xpos,saida_ypos)
call intrpl(2,div,abcissa_neg,ordenada_neg,ptos,saida_xneg,saida_yneg)
do j=1,ptos
   spr_valo_flu(j,R+1)=saida_yneg(j)
   spr_valo_flu(j+ptos,R+1)=saida_ypos(j)
end do

deallocate ( soma0 )
deallocate ( somaL )

deallocate ( ap )
deallocate ( bp )
deallocate ( cp )
deallocate ( abcissa_pos )
deallocate ( abcissa_neg )
deallocate ( ordenada_pos )
deallocate ( ordenada_neg )
deallocate ( saida_ypos )
deallocate ( saida_yneg )

deallocate ( L )
deallocate ( LL )
deallocate ( omega )


end subroutine 

