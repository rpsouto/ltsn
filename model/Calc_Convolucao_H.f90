subroutine convolucao(no,eigval,eigvec,inv_eigvec,Fonte,uo,x,L,c)
!calcula convolucao com fonte do tipo F*exp(-x/uo)

implicit none

integer,intent(in)                              ::no
integer                                       ::i
integer                                       ::j

double precision,intent(in)                        ::uo
double precision,intent(in)                        ::x
double precision,intent(in)                        ::L
double precision,intent(in),dimension(no)            ::Fonte
double precision,intent(in),dimension(no)            ::eigval
double precision,intent(in),dimension(no,no)         ::eigvec
double precision,intent(in),dimension(no,no)         ::inv_eigvec
double precision,intent(out),dimension(no)            ::c
double precision,dimension(no)                     ::aux1
double precision,dimension(no,no)                  ::aux2
double precision,dimension(no)                     ::vec
double precision                              ::deuo
double precision                              ::deL
double precision                              ::cte

aux1=0
aux2=0
vec=0
deuo=dexp(-x/uo)
deL=dexp(-L/uo)

aux1(1:no)=Matmul(inv_eigvec(1:no,1:no),Fonte(1:no))

do i=1,no
   cte=uo/(1.0d+00+eigval(i)*uo)
   if(eigval(i)<0) then
      vec(i)=cte*(dexp(eigval(i)*x)-deuo)
   else
      vec(i)=cte*(dexp(eigval(i)*(x-L))*deL-deuo)
   endif
enddo

do j=1,no
   do i=1,no
      aux2(i,j)=eigvec(i,j)*vec(j)
   enddo
enddo

c(1:no)=Matmul(aux2(1:no,1:no),aux1(1:no))

end