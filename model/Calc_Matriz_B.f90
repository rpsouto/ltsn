subroutine calc_b(n,eigval,eigvec,x,L,b)

integer,intent(in)                           ::n

real(kind=8),intent(in),dimension(n)            ::eigval
real(kind=8),intent(in),dimension(n,n)            ::eigvec
real(kind=8),intent(in)                        ::x
real(kind=8),intent(in)                        ::L
real(kind=8),dimension(n)                     ::vet
real(kind=8),dimension(n,n),intent(out)            ::b

vet=0

do i=1,n
   if(eigval(i).lt.0) then
      vet(i)=dexp(eigval(i)*x)
   else
      vet(i)=dexp(eigval(i)*(x-L))
   endif
enddo

do i=1,n
   do j=1,n
      b(i,j)=eigvec(i,j)*vet(j)
   enddo
enddo

end