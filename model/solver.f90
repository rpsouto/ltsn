SUBROUTINE solver

USE generalparams
EXTERNAL solverSparse

CALL solverSparse(R,no,lv,NM,iwl,phi,hl,cc0,ccL,uo,u,w,z,Chl,wl,a_w,a_c,lastiter,ptos,saida_xneg,saida_xpos,spr_valo_flu)

END SUBROUTINE solver
