
subroutine inv_autovet(n,a)

implicit none


integer,intent(in)                           ::n
integer                                    ::m
integer                                    ::lda
integer                                    ::info
integer                                    ::lwork
integer,dimension(n)                        ::ipiv
real(kind=8),intent(inout),dimension(n,n)         ::a
real(kind=8),dimension(10*n)                  ::work

m=n
lda=n
lwork=n  

call dgetrf(n,m,a,lda,ipiv,info)

call dgetri(n,a,lda,ipiv,work,lwork,info)

end