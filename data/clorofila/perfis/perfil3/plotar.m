clear; close all; hold off

load perfil3.dat;
chla = perfil3(:,2);
z = perfil3(:,1);

figure(1);
plot(chla,-z,'ko-','linewidth',2.0);
xlim([0 max(chla)+1]);
xlim([0 10]);
ylim([-max(z) 0]);
%title(['Gaussian model for chlorophyll concentration profile'],'Fontsize',16);
xlabel('C (mg/m^3)','Fontsize',16,'FontWeight','bold')
%ylabel('geometric depth z (m)','Fontsize',16,'FontWeight','bold')
ylabel('Depth z (m)','Fontsize',16,'FontWeight','bold')
drawnow
%close
