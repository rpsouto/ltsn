clear; close all; hold off

load perfil2.dat;
chla = perfil2(:,2);
z = perfil2(:,1);

figure(1)
%plot(chla,-z,'k-o','linewidth',0.5);
plot(chla,-z);
xlim([0 10]);
ylim([-40.0 0]);
%title(['Chlorophyll concentration profile'],'Fontsize',30);
xlabel('C (mg/m^3)','Fontsize',16,'FontWeight','bold')
ylabel('geometric depth z (m)','Fontsize',16,'FontWeight','bold')
drawnow
print('perfil.eps','-deps')
