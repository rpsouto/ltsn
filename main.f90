PROGRAM main

USE generalparams

IMPLICIT NONE
EXTERNAL readexpdata, solver, printresults

CALL readexpdata

CALL solver

CALL printresults

END PROGRAM main



