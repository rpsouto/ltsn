
!     .. PIM Parameters ..
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
      INTEGER DPARSIZ
      PARAMETER (DPARSIZ=6)
      INTEGER LDA
      PARAMETER (LDA=R*no)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA)
      INTEGER BASIS
      PARAMETER (BASIS=15)
      INTEGER BASIS1
      PARAMETER (BASIS1=2)
      INTEGER LWRK
      PARAMETER (LWRK= (5+2*BASIS)*LOCLEN+2*BASIS)
      INTEGER NGAMMA
      PARAMETER (NGAMMA=13)
!     ..
!     .. Scalars in Common ..
      INTEGER MMM
!     ..
!     .. Arrays in Common ..
      DOUBLE PRECISION MatrizSist(LDA,LOCLEN),GAMMA(NGAMMA),Q1(LOCLEN),Q2(LOCLEN)
!     ..
!     .. Local Scalars ..
      DOUBLE PRECISION TOL
      INTEGER C,MAXIT,N,POLYT,PRET,STOPT,V
!     ..
!     .. Local Arrays ..
      DOUBLE PRECISION DPAR(DPARSIZ),WRK(LWRK)
      INTEGER IPAR(IPARSIZ)
!     ..
!     .. External Functions ..
      DOUBLE PRECISION PDNRM2
      EXTERNAL PDNRM2
!     ..
!     .. External Subroutines ..
      EXTERNAL MATVEC,PDSUM,POLYL,POLYR,PROGRESS,REPORT,TMATVEC

