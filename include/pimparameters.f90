!Definicao de parametros da biblioteca PIM
!integer,parameter   :: iparsiz=13
!integer,parameter   :: dparsiz=6
!integer,parameter   :: lda=R*no
!integer,parameter   :: loclen=lda
!integer,parameter   :: basis=15
!integer,parameter   :: basis1=2
!integer,parameter   :: lwrk= (5+2*basis)*loclen+2*basis
!integer,parameter   :: ngamma=13

!     ..
!     .. Scalars in Common ..
integer            :: mmm

!     ..
!     .. Local Scalars ..
double precision   :: tol
integer            :: c,maxit,n,polyt,pret,stopt,v

!     ..
!     .. Local Arrays ..
double precision   :: dpar(dparsiz)
integer            :: ipar(iparsiz)

!     ..
!     .. External Functions ..
double precision   :: PDNRM2
external PDNRM2

!     ..
!     .. External Subroutines ..
external MATVEC,PDSUM,POLYL,POLYR,PROGRESS,REPORT,TMATVEC


!     ..
!     .. Common blocks ..
common /B0001/gamma,mmm
common /PIMA/MatrizSist
common /PIMQ1/Q1
common /PIMQ2/Q2
!     ..

N = LDA
C = BASIS
V = BASIS1
PRET = 0
STOPT = 1
TOL = 1.0D-10                                 
MAXIT = INT(N/2)
POLYT = 1
MMM = 1

!write(*,*) LDA,BASIS,IPARSIZ,LOCLEN

IPAR(1) = LDA
IPAR(2) = N

