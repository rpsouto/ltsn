
!     .. PIM Parameters ..
      INTEGER IPARSIZ
      PARAMETER (IPARSIZ=13)
      INTEGER DPARSIZ
      PARAMETER (DPARSIZ=6)
      INTEGER LDA
      PARAMETER (LDA=R*no)
      INTEGER LOCLEN
      PARAMETER (LOCLEN=LDA)
      INTEGER BASIS
      PARAMETER (BASIS=15)
      INTEGER BASIS1
      PARAMETER (BASIS1=2)
      INTEGER LWRK
      PARAMETER (LWRK= (5+2*BASIS)*LOCLEN+2*BASIS)
      INTEGER NGAMMA
      PARAMETER (NGAMMA=13)
!     ..
!     .. Scalars in Common ..
      INTEGER MMM
!     ..
!     .. Arrays in Common ..
      DOUBLE PRECISION MatrizSist(LDA,LOCLEN),GAMMA(NGAMMA),Q1(LOCLEN),Q2(LOCLEN)
!     ..
!     .. Local Scalars ..
      DOUBLE PRECISION TOL
      INTEGER C,MAXIT,N,POLYT,PRET,STOPT,V
!     ..
!     .. Local Arrays ..
      DOUBLE PRECISION DPAR(DPARSIZ),WRK(LWRK)
      INTEGER IPAR(IPARSIZ)
!     ..
!     .. External Functions ..
      DOUBLE PRECISION PDNRM2
      EXTERNAL PDNRM2
!     ..
!     .. External Subroutines ..
      EXTERNAL MATVEC,PDSUM,POLYL,POLYR,PROGRESS,REPORT,TMATVEC
!     ..
!     .. Intrinsic Functions ..
!      INTRINSIC INT,SQRT
!     ..
!     .. Common blocks ..
      COMMON /B0001/GAMMA,MMM
      COMMON /PIMA/MatrizSist
      COMMON /PIMQ1/Q1
      COMMON /PIMQ2/Q2
!     ..

	N = LDA
	C = BASIS
	V = BASIS1
	PRET = 0
	STOPT = 1
	TOL = 1.0D-10										   
	MAXIT = INT(N/2)
	POLYT = 1
	MMM = 1

	!write(*,*) LDA,BASIS,IPARSIZ,LOCLEN

	IPAR(1) = LDA
	IPAR(2) = N

