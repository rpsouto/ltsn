!integer,parameter   :: lv=173
!integer,parameter   :: R=2
!integer,parameter   :: no=50
!integer,parameter   :: div=no/2
!integer,parameter   :: ptos=10
!integer,parameter       :: NM=11
 
integer,parameter   :: lv=173
integer,parameter   :: ptos=10
integer,parameter       :: NM=11

double precision,allocatable  :: b0(:,:) 
double precision,allocatable  :: bL(:,:)
double precision,allocatable  :: L(:) 
double precision,allocatable  :: LL(:)
double precision,allocatable  :: omega(:)
double precision,allocatable  :: pol(:,:)      
double precision,allocatable  :: hl(:)            
double precision,allocatable  :: VetorSist(:) 
double precision,allocatable  :: SaiSist(:) 
double precision,allocatable  :: SaiSist_Aux(:)
double precision,allocatable  :: A(:,:)
double precision,allocatable  :: u(:) 
double precision,allocatable  :: w(:)
double precision,allocatable  :: Grande_B0(:,:,:),Grande_BL(:,:,:) 
double precision,allocatable  :: Grande_H0(:,:),Grande_HL(:,:)    
double precision,allocatable  :: eigval(:)                       
double precision,allocatable  :: eigvec(:,:)                    
double precision,allocatable  :: inv_eigvec(:,:)              
double precision,allocatable  :: Hh0(:)                     
double precision,allocatable  :: HhL(:)                   
double precision,allocatable  :: cc0(:)                 
double precision,allocatable  :: ccL(:)               
double precision,allocatable  :: fonte(:)           
double precision,allocatable  :: Fluxo_em_0(:),soma0(:,:)
double precision,allocatable  :: Fluxo_em_L(:),somaL(:,:)
double precision,allocatable  :: z(:),Chl(:),ap(:),bp(:),cp(:)
double precision,allocatable  :: abcissa_pos(:)
double precision,allocatable  :: abcissa_neg(:)
double precision,allocatable  :: ordenada_pos(:)
double precision,allocatable  :: ordenada_neg(:)
double precision,allocatable  :: spr_valo_flu(:,:)

!     ..
!     .. Arrays in Common ..

double precision,allocatable  :: MatrizSist(:,:)
double precision,allocatable  :: gamma(:)
double precision,allocatable  :: Q1(:)
double precision,allocatable  :: Q2(:)
!     ..
!     .. Local Arrays ..
double precision,allocatable   :: wrk(:)



! DGEEQU parameters
double precision,allocatable  :: Rr(:),Cc(:)
real(kind=8)                        ::ROWCND,COLCND,AMAX
integer                              ::INFO
! DGEEQU parameters

double precision   :: part1,part2,cte,a_w(NM),a_c(NM),wl(NM)

integer   :: R
integer   :: no
integer   :: div

!Definicao de parametros da biblioteca PIM
integer   :: iparsiz=13
integer   :: dparsiz=6
!integer   :: lda=R*no
integer   :: lda
!integer   :: loclen=lda
integer   :: loclen
integer   :: basis=15
integer   :: basis1=2
!integer   :: lwrk= (5+2*basis)*loclen+2*basis
integer   :: lwrk
integer   :: ngamma=13
!Definicao de parametros da biblioteca PIM

integer                     ::i,m,j,ii,jj,ij,ji
integer                     ::mm,ipath
double precision            ::pi,valor_tau
double precision            ::uo
double precision              :: phi

