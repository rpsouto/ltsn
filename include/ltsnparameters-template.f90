integer,parameter   :: lv=173
integer,parameter   :: R=REGIONS
integer,parameter   :: no=50
integer,parameter   :: div=no/2
integer,parameter   :: ptos=10
integer,parameter       :: NM=11
 
double precision            ::b0(no,no)
double precision            ::bL(no,no)
double precision            ::L(R),LL(R+1)
double precision            ::phi
double precision            ::omega(R)
double precision            ::pol(lv+1,no+2)      
double precision            ::hl(lv+1)         
double precision            ::VetorSist(R*no)
double precision            ::SaiSist(R*no)
double precision            ::SaiSist_Aux(no)
double precision            ::A(no,no)
double precision            ::u(no+1)
double precision            ::w(no)
double precision            ::Grande_B0(R,no,no),Grande_BL(R,no,no)
double precision            ::Grande_H0(R,no),Grande_HL(R,no)
double precision            ::uo
double precision            ::eigval(no)
double precision            ::eigvec(no,no)
double precision            ::inv_eigvec(no,no)
double precision            ::Hh0(no)
double precision            ::HhL(no)
double precision            ::cc0(no)
double precision            ::ccL(no)
double precision            ::fonte(no)
double precision            ::Fluxo_em_0(no),soma0(R,no)
double precision            ::Fluxo_em_L(no),somaL(R,no)
integer                     ::i,m,j,ii,jj,ij,ji
integer                     ::mm,ipath
double precision            ::pi,valor_tau
