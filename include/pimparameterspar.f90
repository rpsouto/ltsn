!Definicao de parametros da biblioteca PIM
integer,parameter   :: iparsiz=13
integer,parameter   :: dparsiz=6
integer,parameter   :: lda=R*no
integer,parameter   :: loclen=lda
integer,parameter   :: basis=15
integer,parameter   :: basis1=2
integer,parameter   :: lwrk= (5+2*basis)*loclen+2*basis
integer,parameter   :: ngamma=13

!     ..
!     .. Scalars in Common ..
integer            :: mmm
!     ..
!     .. Arrays in Common ..

double precision   :: MatrizSist(lda,loclen)
double precision   :: gamma(ngamma),Q1(loclen),Q2(loclen)

!     ..
!     .. Local Scalars ..
double precision   :: tol
integer            :: c,maxit,n,polyt,pret,stopt,v

!     ..
!     .. Local Arrays ..
double precision   :: dpar(dparsiz),wrk(lwrk)
integer            :: ipar(iparsiz)

!     ..
!     .. External Functions ..
double precision   :: PDNRM2
external PDNRM2

!     .. External Subroutines ..
external MATVEC,PDSUM,POLYL,POLYR,PROGRESS,REPORT,TMATVEC

!     ..
!     .. Common blocks ..
common /B0001/gamma,mmm
common /PIMA/MatrizSist
common /PIMQ1/Q1
common /PIMQ2/Q2
!     
