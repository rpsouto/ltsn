MODULE generalparams

integer,parameter   :: ptos=10
integer,parameter   :: NM=11

double precision   :: wl(NM),a_w(NM),a_c(NM)

integer            :: R,no,lv,iwl,lastiter

double precision             :: phi,uo
double precision,allocatable :: hl(:)
double precision,allocatable :: cc0(:)
double precision,allocatable :: ccL(:)
double precision,allocatable :: u(:)
double precision,allocatable :: w(:)
double precision,allocatable :: z(:)
double precision,allocatable :: Chl(:)
double precision,allocatable :: saida_xpos(:)
double precision,allocatable :: saida_xneg(:)
double precision,allocatable :: spr_valo_flu(:,:)

END MODULE generalparams